//
//  ViewController.swift
//  AstraLinkHM
//
//  Created by Benjamin Aballay on 24/05/2017.
//  Copyright © 2017 Benjamin Aballay. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import SDWebImage

class ViewController: UIViewController,UITableViewDataSource,GitProviderDelegate {

    @IBOutlet weak var txtSelectedPeriod: UITextField!
    
    @IBOutlet weak var tblView: UITableView!
    var currentSelectedPeriod : Int = 2;
    var data : GitResponse
    var provider : GitProvider
    var firstData : Bool = true;
    var haveNextLink : Bool = false;
    
    required init(coder aDecoder: NSCoder) {
        
        print("init coder")
        self.data = GitResponse()!
        self.provider = GitProvider()!
        super.init(coder: aDecoder)!
        self.provider.delegate = self;
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.provider.initAuthetication()
        updateTableDataWithSelectedIndex(index: currentSelectedPeriod)
        // Do any additional setup after loading the view, typically from a nib.
    }

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func btnSelectPeriodTouchUpInside(_ sender: Any) {
        ActionSheetMultipleStringPicker.show(withTitle: "Select Wanted Period", rows: [
            ["Last Day Repos", "Last Week Repos", "Last Month Repos"],
            ], initialSelection: [self.currentSelectedPeriod], doneBlock: {
                picker, indexes, values in
                
                self.txtSelectedPeriod.text = (values as! [String]).first
                self.updateTableDataWithSelectedIndex(index: indexes?.first as! Int);
                
                
                return
        }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
        
    }
    
    func updateTableDataWithSelectedIndex(index:Int){
        self.currentSelectedPeriod = index
        self.firstData = true
        switch self.currentSelectedPeriod {
        case 0: // last day
            provider.getDayData();
            break;
        case 1: // last week
            provider.getWeekData()
            break;
        case 2: // last month
            provider.getMonthData()
            break;
        default:
            break;
        }
    }
    
    // MARK: - Table view data source
    
     func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
      func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data.items.count
    }
    
      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellIdentifier", for: indexPath)
        
        let item = self.data.items[indexPath.row]
        let lblUserName  = cell.viewWithTag(1) as! UILabel
        let lblDesc = cell.viewWithTag(2)as! UILabel
        let lblStars = cell.viewWithTag(3)as! UILabel
        let imgView = cell.viewWithTag(4)as! UIImageView
        lblUserName.text = item.owner.userName
        lblDesc.text = item.description
        lblStars.text = String.init(format: "Number Of Stars %d", item.numberOfStarts)
        if (item.owner.avatar_url != ""){
            imgView.sd_setImage(with: URL(string: item.owner.avatar_url), placeholderImage: UIImage(named: "placeholder.png"))

        }
        
        if indexPath.row == self.data.items.count - 1 {
            self.loadMore()
        }
        
        return cell
    }
    
      func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return ""
    }

    func loadMore() {
        if (self.haveNextLink){
            
        }
    }
    
    // MARK: - Git Provider Delegate

    func gitProviderFinishedWithData(data : GitResponse){
        firstData = false;
        if (data.nextPage == ""){
            self.haveNextLink = false;
            self.data.nextPage = "";
        }else{
            self.haveNextLink = true;
            self.data.nextPage = data.nextPage;
        }
        
        DispatchQueue.main.async {
            
            self.data.items.append(contentsOf: data.items)
            self.tblView.reloadData()
        }
        self.tblView.reloadData()
    }
}

