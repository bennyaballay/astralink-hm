//
//  RepoData.swift
//  AstraLinkHM
//
//  Created by Benjamin Aballay on 26/05/2017.
//  Copyright © 2017 Benjamin Aballay. All rights reserved.
//

import Foundation
import ObjectMapper

class RepoData : Mappable{
    var owner : OwnerData
    var description : String = "empty description"
    var numberOfStarts : Int = 0

    required init?(map: Map) {
        self.owner = OwnerData(map: map)!
    }

    func mapping(map: Map) {
        self.description   <- map["description"]
        self.numberOfStarts <- map["stargazers_count"]
        self.owner <- map["owner"]
    }
}
