//
//  GitResponse.swift
//  AstraLinkHM
//
//  Created by Benjamin Aballay on 26/05/2017.
//  Copyright © 2017 Benjamin Aballay. All rights reserved.
//

import Foundation
import ObjectMapper

class GitResponse: Mappable {
    var total_count : Int = 0
    var items : Array<RepoData> = []
    var nextPage : String = ""
    var prevPage : String = ""
    
    required init?(map: Map) {}
    
    required init?(){}
    
    func mapping(map: Map) {
        self.total_count     <- map["total_count"]
        self.items <- map["items"]
        
    }
}
