//
//  OwnerData.swift
//  AstraLinkHM
//
//  Created by Benjamin Aballay on 27/05/2017.
//  Copyright © 2017 Benjamin Aballay. All rights reserved.
//

import Foundation
import ObjectMapper

class OwnerData : Mappable{
    var userName : String = ""
    var avatar_url : String = ""
    
    required init?(map: Map) {}

    func mapping(map: Map) {
        self.userName     <- map["owner"]["login"]
        self.avatar_url <- map["owner"]["avatar_url"]
    }

}
