//
//  GitProvider.swift
//  AstraLinkHM
//
//  Created by Benjamin Aballay on 24/05/2017.
//  Copyright © 2017 Benjamin Aballay. All rights reserved.
//

import Foundation

import Alamofire
import SwiftyJSON
import ObjectMapper

protocol GitProviderDelegate {
    func gitProviderFinishedWithData(data : GitResponse)
}

class GitProvider {
    
    var delegate : GitProviderDelegate?
    
    required init?(){
    
    }
    
    required init?(delegate : GitProviderDelegate){
        self.delegate = delegate;
    }
    
    func getCurrentDate() -> String{
        let date = Date()
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let strDate = dateFormatter.string(from:date)
        
        
        return strDate
    }
    func initAuthetication(){
        let URL = "https://api.github.com/user?access_token=70b8d4c9f7ffcefce7362fa4b7518c929bad2732"
        
        Alamofire.request(URL).responseJSON { (result) in
            print(result);
        }
    }
    
    func executeUrlRequest(request : URLRequest) {
        
    }
    
     func getMonthData(){
//        let strDate = getCurrentDate()
//        let URL = String.init(format:"https://api.github.com/search/repositories?q=created:>\(strDate) -v-1m")
        let URL = String.init(format:"https://api.github.com/search/repositories?q=created:>2014-05-28 -v-1m")
        let escapedAddress = URL.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)


        Alamofire.request(escapedAddress!).responseJSON { (result) in
            let event =  Mapper<GitResponse>().map(JSONObject: result.value)
            let headers = result.response?.allHeaderFields["Link"]
//            event?.prevPage = result.response?.allHeaderFields[""]
//            event?.prevPage = result.response?.allHeaderFields[""]
            self.delegate?.gitProviderFinishedWithData(data: event!);
        }
    }
    
     func getWeekData(){
        //        let strDate = getCurrentDate()
        //        let URL = String.init(format:"https://api.github.com/search/repositories?q=created:>\(strDate) -v-1m")
        let URL = String.init(format:"https://api.github.com/search/repositories?q=created:>2016-05-26 -v-1m")
        let escapedAddress = URL.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        
        
        Alamofire.request(escapedAddress!).responseJSON { (result) in
            let event =  Mapper<GitResponse>().map(JSONObject: result.value)
        }

    }
    
     func getDayData(){
        //        let strDate = getCurrentDate()
        //        let URL = String.init(format:"https://api.github.com/search/repositories?q=created:>\(strDate) -v-1m")
        let URL = String.init(format:"https://api.github.com/search/repositories?q=created:>2016-05-26 -v-1m")
        let escapedAddress = URL.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        
        
        Alamofire.request(escapedAddress!).responseJSON { (result) in
            let event =  Mapper<GitResponse>().map(JSONObject: result.value)
        }

    }
    
     func getNextPageData(){
        
    }
}

